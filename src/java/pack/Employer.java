/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack;
import java.util.*;
import anotation.*;
import model.ModelView;
/**
 *
 * @author iitu
 */
public class Employer {
    String nom;
    Date naissance;
    Float salaire;
    int etat;

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public Float getSalaire() {
        return salaire;
    }

    public void setSalaire(Float salaire) {
        this.salaire = salaire;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    public String getNom(){
        return nom;
    }
    public void setNom(String anarana){
        nom = anarana;
    }
    
    @Url(name="all")
    public ModelView getAll (){
        ModelView retour = new ModelView();
        HashMap<String, Object> value = new HashMap<String, Object>();
        Employer e = new Employer();
        e.setNom("Bogoss 2022");
        value.put("retour", e);
        retour.setPageJsp("accueil.jsp");
        retour.setValue(value);
        return retour;
    }
    @Url(name="take")
    public ModelView insertForExample(){
        ModelView retour = new ModelView();
        retour.setPageJsp("go.jsp");
        return retour;
    }
    
}
